# ui.wren
ui framework for the wren language, made w/ https://domeengine.com/

more info at https://zvava.org/wiki/ui-wren.html

## debugging
engine  version: 1.8.2

`dome.exe` -- run on windows to run game
`./dome` -- run on linux to run game
`./dome-osx` -- run on mac os to run game
