import "math" for Vector, Math
import "graphics" for Canvas, Color, Font

var Ubuntu = {
	"regular": Font.load("regular", "font/Ubuntu-Regular.ttf", 16),
	"italic": Font.load("ubuntu-italic", "font/Ubuntu-Italic.ttf", 16),
	"bold": Font.load("ubuntu-bold", "font/Ubuntu-Bold.ttf", 16),
	"italic": Font.load("ubuntu-bold-italic", "font/Ubuntu-BoldItalic.ttf", 16)
}

for (variant in Ubuntu) variant.value.antialias = true
Canvas.font = "regular"

// enumerators
class Enum {
	construct new(v, n) {
		_v = v
		_n = n
	}

	getName() { _n }
	get() { _v }
	==(v) { v.get() == _v }
}

class Orientation is Enum {
	construct new(v, n) { super(v, n) }

	static horizontal { Orientation.new(0, "horizontal") }
	static landscape  { Orientation.new(0, "horizontal") }
	static vertical   { Orientation.new(1, "vertical") }
	static portrait   { Orientation.new(1, "vertical") }
}

class Position is Enum {
	construct new(v, n) { super(v, n) }

	// align to start of parent
	static start  { Position.new(0, "start") }
	static begin  { Position.new(0, "start") }
	// align in the middle of parent
	static center { Position.new(0.5, "center") }
	static middle { Position.new(0.5, "center") }
	// align to end of parent
	static end    { Position.new(1, "end") }
}

// perimeter
class Perimeter {
	// side widths
	top { _top }
	top=(v) { _top = v }
	right { _right }
	right=(v) { _right = v }
	bottom { _bottom }
	bottom=(v) { _bottom = v }
	left { _left }
	left=(v) { _left = v }
	// get biggest side
	max { Math.max(Math.max(Math.max(top, right), bottom), left) }

	construct new(t) { set(t) }
	construct new(t, r) { set(t, r) }
	construct new(t, r, b) {
		set(t, r)
		bottom = b
	}
	construct new(t, r, b, l) { set(t, r, b, l) }

	construct zero() { reset() }
	reset() { set(0) }

	set(v) {
		if (!(v is Num)) v = 0
		top = right = bottom = left = v
	}

	set(t, r) {
		top = t
		right = r
		bottom = t
		left = r
	}

	set(t, r, b, l) {
		top = t
		right = r
		bottom = b
		left = l
	}
}

class Border is Perimeter {
	// side colors
	topColor { _topColor }
	topColor=(v) { _topColor = v }
	rightColor { _rightColor }
	rightColor=(v) { _rightColor = v }
	bottomColor { _bottomColor }
	bottomColor=(v) { _bottomColor = v }
	leftColor { _leftColor }
	leftColor=(v) { _leftColor = v }
	// helper
	color { topColor || rightColor || bottomColor || leftColor }
	colors { [topColor, rightColor, bottomColor, leftColor] }
	color=(v) { _color = topColor = rightColor = bottomColor = leftColor = v }

	construct new(t, c) {
		super(t)
		_color = c
	}
	construct new(t, r, c) {
		super(t, r)
		_color = c
	}
	construct new(t, r, b, c) {
		super(t, r, b)
		_color = c
	}
	construct new(t, r, b, l, c) {
		super(t, r, b, l)
		_color = c
	}

	construct zero() { super() }
	reset() { set(0, null) }

	set(v, c) {
		if (!(v is Num)) v = 0
		top = right = bottom = left = v
		color = c
	}

	set(t, r, c) {
		top = t
		right = r
		bottom = t
		left = r
		color = c
	}

	set(t, r, b, l, c) {
		top = t
		right = r
		bottom = b
		left = l
		color = c
	}
}

// base ui element
class UIElement {
	// metadata
	name { _name }
	parent { _parent }
	children { _children }
	tags { _tags }

	parent=(p) {
		if (p.children.contains(this)) {
			_parent = markUpdate(p)
		}
	}

	// layout
	x     { _x }
	x=(v) {
		if (x != v) return (_x = markUpdate(v))
	}
	y     { _y }
	y=(v) {
		if (y != v) return (_y = markUpdate(v))
	}

	move(x, y) {
		_x = x
		_y = y
	}

	w     { _w }
	w=(v) {
		if (w != v) return (_w = markUpdate(v))
	}
	h     { _h }
	h=(v) {
		if (h != v) return (_h = markUpdate(v))
	}

	resize(w, h) {
		_w = w
		_h = h
		updates = 1
	}

	padding     { _padding }
	padding=(v) {
		if (padding != v) return (_padding = markUpdate(v))
	}
	margin     { _margin }
	margin=(v) {
		if (margin != v) return (_margin = markUpdate(v))
	}
	border     { _border }
	border=(v) {
		if (border != v) return (_border = markUpdate(v))
	}

	// layout helpers
	innerX { x + padding.left }
	innerY { y + padding.top }
	innerW { w - padding.left - padding.right }
	innerH { h - padding.top - padding.bottom }

	outerX { x - border.left }
	outerY { y - border.top }
	outerW { border.left + w +  border.right }
	outerH { border.top + h + border.bottom }

	boundX { x - border.left - margin.left }
	boundY { y - border.top - margin.top }
	boundW { margin.left + border.left + w  + border.right  + margin.right }
	boundH { margin.top + border.top + h + border.bottom + margin.bottom }

	// style
	bg     { _bg }
	bg=(v) {
		if (bg != v) return (_bg = markUpdate(v))
	}
	fg     { _fg }
	fg=(v) {
		if (fg != v) return (_fg = markUpdate(v))
	}

	// updates system
	updates     {
		if (_children != null) {
			var m = 0 // biggest updates of children
			children.each {|child| m = Math.max(m, child.updates) }

			return Math.max(_updates, m)
		} else {
			return _updates
		}
	}

	updates=(v) { _updates = v }

	markUpdate(v) {
		_updates = 1
		return v
	}

	construct new(name, parent, children) {
		_updates = 1
		_name = name || "untitled ui element"
		_parent = parent || null
		_children = children || null
		_tags = []

		if (children != null) {
			var i = 0
			children.each {|child|
				if (child is String) {
					children[i] = Text.new(child)
				}

				if (children[i].parent != this) children[i].parent = this
				i = i + 1
			}
		}

		_x = 0
		_y = 0
		_w = 100
		_h = 100
		_padding = Perimeter.zero()
		_margin = Perimeter.zero()
		_border = Border.zero()
		_bg = null
		_fg = Color.white
	}

	addChildren(cs) { cs.each {|c| addChild(c)} }
	addChild(c) {
		if (children == null || children.contains(c)) return System.print("%(toString()) -- cannot add child %(c.toString())")

		children.add(c)
		c.parent = this
	}

	removeChild(c) {
		if (children == null || children.contains(c)) return System.print("%(toString()) -- cannot remove child %(c.toString())")

		children.remove(c)
		c.parent = null
	}

	draw() {
		if (_updates > 0) _updates = _updates - 1
		if (bg != null) Canvas.rectfill(x, y, w, h, bg)
		if (border.max > 0 && border.color != null) {
			for (i in 0..border.max) {
				if (border.topColor != null    && border.top - i > 0)    Canvas.line(x - i,     y - i     - 1, x + w + i,     y - i     - 1, border.topColor, 1)
				if (border.rightColor != null  && border.right - i > 0)  Canvas.line(x + w + i, y - i     - 1, x + w + i,     y + h + i - 1, border.rightColor, 1)
				if (border.bottomColor != null && border.bottom - i > 0) Canvas.line(x + w + i, y + h + i - 1, x - i,         y + h + i - 1, border.bottomColor, 1)
				if (border.leftColor != null   && border.left - i > 0)   Canvas.line(x - i,     y + h + i - 1, x - i,         y - i     - 1, border.leftColor, 1)
			}
		}
	}

	toString() { name }

	printChildren() { printChildren(1) }

	printChildren(indent) {
		System.print(("| " * indent) + toString())

		if (children != null) {
			for (child in children) child.printChildren(indent + 1)
		}
	}
}

class Panel is UIElement {
	alignH { _alignH }
	alignH=(v) { _alignH = v }
	alignV { _alignV }
	alignV=(v) { _alignV = v }

	construct new(child) {
		super("panel", null, [child])
		bg = Color.hex("1c1c1c")
		padding.set(8)
		w = child.boundW
		h = child.boundH
	}

	construct new() {
		super("panel", null, [])
		bg = Color.hex("1c1c1c")
		padding.set(8)
		w = h = 100
	}

	draw() {
		super.draw()

		for (child in children) {
			child.x = innerX + child.margin.left - child.margin.right + (innerW / 2) - (child.outerW / 2)
			child.y = innerY + child.margin.top - child.margin.bottom + (innerH / 2) - (child.outerH / 2)

			child.draw()
		}
	}
}

class Stack is UIElement {
	direction     { _direction }
	direction=(v) { _direction = v }
	align     { _align }
	align=(v) { _align = v }
	childPadding { _childPadding }
	childPadding=(v) { _childPadding = v }

	autoSize { _autoSize }
	autoSize=(v) { _autoSize = v }

	init() {
		_direction = Orientation.vertical
		_align = Position.center
		_childPadding = 12
		_autoSize = true
		padding.set(12)
		bg = Color.hex("2c2c2c")
	}

	construct new(orientation, position, children) {
		super("stack", null, children)
		init()
		direction = orientation
		align = position
	}

	construct new(orientation, children) {
		super("stack", null, children)
		init()
		direction = orientation
	}

	construct new(children) {
		super("stack", null, children)
		init()
	}

	construct new() {
		super("stack", null, [])
		init()
	}

	draw() {
		super.draw()
		if (children.count == 0) return

		if (direction == Orientation.vertical) { // column |
			var total = childPadding * -1 // total height of children
			var maxW = 0 // maximum width of children
			for (c in children) {
				total = total + c.boundH + childPadding
				maxW = Math.max(maxW, c.boundW)
			}
			if (autoSize) { // update size of container
				w = padding.left + maxW + padding.right
				h = padding.top + total + padding.bottom
			}

			var cursorX = x + (w / 2)
			var cursor = y + (h / 2) - (total / 2)
			for (child in children) { // iterate over children
				child.x = cursorX + child.margin.left - child.margin.right + child.border.left - child.border.right - (child.w / 2)
				child.y = cursor
				child.draw()

				cursor = cursor + child.boundH + childPadding
			}
		} else { // row —
			var total = childPadding * -1
			var maxH = 0
			for (c in children) {
				total = total + c.boundW + childPadding
				maxH = Math.max(maxH, c.boundH)
			}
			if (autoSize) { // update size of container
				h = padding.top + maxH + padding.bottom
				w = padding.left + total + padding.right
			}
			// cursor that determines where the next child will be placed
			var cursorY = y + (h / 2)
			var cursor = x + (w / 2) - (total / 2)
			for (child in children) { // move and draw children
				child.y = cursorY + child.margin.top - child.margin.bottom + child.border.top - child.border.bottom - (child.h / 2)
				child.x = cursor
				child.draw() // draw after moving child to cursor position

				cursor = cursor + child.boundW + childPadding
			}
		}
	}

	resize(w, h) {
		super.resize(w, h)
		autoSize = false
	}

	resize(w, h, a) {
		super.resize(w, h)
		autoSize = a
	}

	toString() {
		return "%(name) -- %(direction.getName()) %(align.getName())"
	}
}

class Text is UIElement {
	font     { _font }
	font=(v) { _font = markUpdate(v) }
	text     { _text }
	text=(v) {
		_text = markUpdate(v)
		_lines = v.split("\n").count
		var b = font.getArea(v)
		w = b.x
		h = b.y - 38
	}

	construct new(t) {
		super("text", null, null)
		_font = Ubuntu["regular"]
		text = t
		//bg = Color.black // show text bounding box for debug
	}

	draw() {
		super.draw()

		Canvas.print(text, x, y - 38, fg)
	}

	toString() {
		return "text -- \"%(text)\""
	}
}
