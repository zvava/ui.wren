import "dome" for Version, Process, Window
import "graphics" for Canvas, Font, Color
import "input" for Mouse, Keyboard
import "ui" for Orientation, Position, Stack, Panel, Text

class Main {
	construct new() {}

	init() {
		_WIDTH = 500
		_HEIGHT = 500

		_wrapper = Stack.new(Orientation.vertical, [
			Stack.new(Orientation.horizontal, [
				Panel.new([ "oh boy :D" ]),
				Panel.new([ "oh no, increasing\nthe no. of lines..."]),
				Panel.new([ "it's okay\nit works fine\nno problem (phew)"])
			]),
			Stack.new(Orientation.horizontal, [
				Panel.new([ "wow it\nactualy works" ]),
				Panel.new([ ":>" ]),
				Panel.new([ "that's nice" ])
			])
		])
		_wrapper.children[0].children[1].w = 200
		_wrapper.children[1].children[1].h = 200
		_wrapper.w = _WIDTH
		_wrapper.h = _HEIGHT
		_wrapper.padding = 16
		_wrapper.printChildren()

		Window.title = "ui demo"

		Canvas.resize(_WIDTH, _HEIGHT)

		_font = Font.load("ubuntu", "font/Ubuntu-Regular.ttf", 16)
		_font.antialias = true
		Canvas.font = "ubuntu"
	}

	update() {
		if (Keyboard.isKeyDown("left ctrl")) {
			if (Keyboard.isKeyDown("q")) Process.exit()
			if (Keyboard.isKeyDown("f")) { } // TODO: fullscreen
		}

		if (Window.width != _WIDTH || Window.height != _HEIGHT) {
			_wrapper.w = _WIDTH = Window.width
			_wrapper.h = _HEIGHT = Window.height
			Canvas.resize(_WIDTH, _HEIGHT)
		}
	}

	draw(dt) {
		var u
		if ((u = _wrapper.updates) > 0) {
			_rest = 0
			Canvas.cls()
			_wrapper.draw()
			Canvas.print(u, 0, -16, u > 0 ? Color.red : Color.green)
			Canvas.print(_wrapper.updates, 0, 0, _wrapper.updates > 0 ? Color.red : Color.green)
		} else {
			_rest = _rest + 1
			Canvas.print(_rest, 0, -32, Color.white)
		}
	}
}

var Game = Main.new()
